#!/bin/sh

# When this exits, exit all back ground process also.
trap 'kill $(jobs -p)' EXIT

if [ ] ; then
# iterate through the each given file names,
for file in "$@"
do
	# show tails of each in background.
	tail -f $file &
done
fi

tail -F /var/log/kern.log &
tail -F /var/log/auth.log &

# wait .. until CTRL+C
wait
