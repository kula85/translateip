#!/usr/bin/env python

# TODO
# 1. only check for "Failed password", what about failed rsa?

import sys
import syslog
import os
import getopt
import subprocess
import time
import re
import datetime
import dateutil.parser
import logging.handlers

load_balancer_ip = "128.4.31.216"

infolog = "/m/log/auth"

lblog = {"hoek": "/var/log/go-balance-hoek.eecis.udel.edu",
        "linuxlab":"/var/log/go-balance-linuxlab.acad.ece.udel.edu",
        "mlb":"/var/log/go-balance-mlb.acad.ece.udel.edu",
        "mudskipper":"/var/log/go-balance-mudskipper.eecis.udel.edu",
        "orioles":"/var/log/go-balance-orioles.acad.ece.udel.edu",
        "ren":"/var/log/go-balance-ren.eecis.udel.edu",
        "stimpy":"/var/log/go-balance-stimpy.eecis.udel.edu",
        "yankees":"/var/log/go-balance-yankees.acad.ece.udel.edu"}

logfiledb = {}
hostname_logfile_dic = {}

def get_file_handle_for_name(hostname, forcereopenfile):
  if not forcereopenfile:
    for k,v in logfiledb.items():
      if hostname.startswith(k):
        return v

  logfile = ''
  thekey = ''
  for k,v in lblog.items():
    if hostname.startswith(k):
      logfile = v
      thekey = k
      break
  assert logfile != ''
  assert thekey != ''
  logfilehandle = logging.handlers.WatchedFileHandler(logfile, "r")
  logfilehandle.seek(0,os.SEEK_END) # Go to the end of the file, then go back a little

  logfiledb[thekey] = logfilehandle
  return logfilehandle

# lazily open file stream
def get_file_handle(hostname, forcereadfromlogfiledb):
  if not forcereadfromlogfiledb:
    if hostname_logfile_dic.has_key(hostname):
      return hostname_logfile_dic[hostname]

  fh = get_file_handle_for_name(hostname, forcereadfromlogfiledb)
  hostname_logfile_dic[hostname] = fh

  return fh

def follow(thefile):
  thefile.seek(0,2)
  while True:
    line = thefile.readline()
    if not line:
      time.sleep(1) # tail -f sleep 1 sec before next iteration
      continue
    yield line

def get_prev_line(fh):
  numofnewline = 0
  while numofnewline < 2:
    fh.seek(-1, os.SEEK_CUR)
    if fh.read(1) == '\n':
      numofnewline = numofnewline + 1
    fh.seek(-1, os.SEEK_CUR)
  fh.seek(1, os.SEEK_CUR)

  pos = fh.tell()
  line = fh.readline().rstrip('\n')
  fh.seek(pos)
  return line

def main():
  #syslog.openlog("translateip", syslog.LOG_PID, syslog.LOG_AUTH)
  # "Mar  7 12:08:09 mlb1.acad.ece.udel.edu sshd[8166]: Failed password for invalid user test from 128.4.31.216 port 33211 ssh2"
  infore = re.compile(r"^(?P<date>[\w: ]{16})(?P<host>[^ ]+).+sshd\[\d+\]:.+port (?P<port>\d+) ssh2")
  # "INFO: CONN #0014 2015/03/14 22:07:02.892857 Opened 5.141.207.232:61634 <tcp> 128.4.31.32:22 <pipe> 128.4.31.216:33211 <tcp> 128.4.26.22:22"
  # "INFO: CONN #12201 2015/08/05 04:37:14.131226 Opened 124.117.225.163:38530 <tcp> 128.4.31.42:22 <pipe> 128.4.31.216:45129 <tcp> 128.4.31.146:22"
  lbre = re.compile(r"INFO: CONN #\d+ (?P<date>[\w:/ ]{19}).+Opened (?P<realip>[0-9.]+):.+ <tcp>.+<pipe> " + load_balancer_ip + r":(?P<port>[0-9]+) <tcp>.+")

  #infologfile = open(infolog, "r")
  infologfile = logging.handlers.WatchedFileHandler(infolog, "r")
  infologfile.seek(0,2) # Go to the end of the file

  for line in follow(infologfile):
    if line.find("Failed password") != -1 and line.find(load_balancer_ip) != -1: # load_balancer_ip is a must since the only purpose of this script to translate such case.
      # parse line for 1. time, 2. port 3. original dest host(mlb, hoek, ...)
      m = infore.match(line)

      # using go-balance-hoek.log or go-balance-mlb.log file handler depending on 3 above
      date = dateutil.parser.parse(m.group('date')) # parse str into datetime obj
      hostname = m.group('host')
      port = m.group('port')

      fh = get_file_handle(hostname, False)
      fh.seek(0,os.SEEK_END) # seek to the end

      # seek back to the 1. time
      m2 = None
      while not m2 or (date - dateutil.parser.parse(m2.group('date')).replace(microsecond=0)) < datetime.timedelta():
        m2 = lbre.match(get_prev_line(fh))

      # start looking for the first entry using 2. port
      fh.readline()
      m1 = None
      timedeltatoolarge = False
      while not m1 or m1.group('port') != port:
        m1 = lbre.match(get_prev_line(fh))
        if m1 and (date - dateutil.parser.parse(m1.group('date')).replace(microsecond=0)) > datetime.timedelta(seconds=300): # if it take more than 5 minutes, something is wrong.
          timedeltatoolarge = True
          break

      if timedeltatoolarge:
        #print("test: translation fail to find matching entry: " + line)
        pass
      else:
        # do translation, output auth log entry
        #syslog.syslog(syslog.LOG_WARNING, "Failed password for invalid user test from " + m1.group('realip') + " port 12345 ssh2")
        #syslog.syslog(syslog.LOG_WARNING, re.sub(r"from.+port", "from " + m1.group('realip') + " port", line))
        print(re.sub(r"from.+port", "from " + m1.group('realip') + " port", line))

if __name__ == '__main__':
    sys.exit(main())
